#include <atomic>
#include <iostream>
#include <thread>
#include <vector>

int FetchAndIncrement(std::atomic<int>& value) {
  // CAS loop
  while (true) {
    // Read current state
    int curr_value = value.load();

    // Try to commit changes
    if (value.compare_exchange_weak(curr_value, curr_value + 1)) {
      return curr_value;
    }

    // Repeat on failure
  }
}

static const size_t kThreads = 5;
static const size_t kIncrementsPerThread = 1005001;

int main() {
  std::atomic<int> shared_counter{0};

  std::vector<std::thread> threads;

  for (size_t i = 0; i < 5; ++i) {
    threads.emplace_back([&]() {
      for (size_t j = 0; j < kIncrementsPerThread; ++j) {
        FetchAndIncrement(shared_counter);
      }
    });
  }

  for (auto& thread : threads) {
    thread.join();
  }

  std::cout << "Shared counter value: " << shared_counter.load()
            << ", expected: " << kThreads * kIncrementsPerThread << std::endl;
}
