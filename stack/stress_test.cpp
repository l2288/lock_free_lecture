#include <twist/test/test.hpp>
#include <twist/test/assert.hpp>

#include <twist/stdlike/mutex.hpp>

#include <twist/test/util/lockfree.hpp>
#include <twist/test/util/race.hpp>

#include <twist/fault/adversary/lockfree.hpp>

#include "treiber_stack.hpp"
#include "treiber_stack_pool.hpp"
#include "blocking_stack.hpp"

TEST_SUITE(LockFreeExamples) {
  TWIST_TEST_TL(Stack, 3s) {
    // twist::fault::SetAdversary(twist::fault::CreateLockFreeAdversary());

    twist::test::util::ReportProgressFor<lockfree::TreiberStack<int>>
        stack;

    twist::test::util::Race race;

    std::atomic<int> pushed{0};
    std::atomic<int> popped{0};

    static const int kThreads = 5;

    for (int i = 0; i < kThreads; ++i) {
      race.Add([&, i]() {
        int value = i;
        while (twist::test::KeepRunning()) {
          pushed += value;
          stack->Push(value);

          value = *(stack->TryPop());
          popped += value;
        }
      });
    }

    race.Run();

    std::cout << "Pushed: " << pushed.load() << std::endl;
    std::cout << "Popped: " << popped.load() << std::endl;

    ASSERT_EQ(pushed.load(), popped.load());
  }
}

RUN_ALL_TESTS();
