#pragma once

#include <twist/stdlike/atomic.hpp>

#include <optional>

namespace lockfree {

// Treiber lock-free stack + caching allocator

template <typename T>
class TreiberStackWithPool {
  struct Node {
    std::optional<T> value;
    Node* next{nullptr};
  };

  class NodeStack {
   public:
    ~NodeStack() {
      DeleteList(top_.load());
    }

    void Push(Node* node) {
      node->next = top_.load();
      while (!top_.compare_exchange_weak(node->next, node)) {
        ;
      }
    }

    Node* TryPop() {
      while (true) {
        Node* curr_top = top_.load();
        if (curr_top == nullptr) {
          return nullptr;
        }
        if (top_.compare_exchange_weak(curr_top, curr_top->next)) {
          return curr_top;
        }
      }
    }

   private:
    static void DeleteList(Node* head) {
      while (head != nullptr) {
        Node* to_delete = head;
        head = head->next;
        delete to_delete;
      }
    }

   private:
    twist::stdlike::atomic<Node*> top_{nullptr};
  };

  class NodeAllocator {
   public:
    Node* AllocateNode() {
      Node* node = pool_.TryPop();
      if (node != nullptr) {
        return node;
      }
      return new Node{};
    }

    void Free(Node* node) {
      pool_.Push(node);
    }

   private:
    NodeStack pool_;
  };

 public:
  void Push(T value) {
    Node* node = allocator_.AllocateNode();
    node->value.emplace(std::move(value));
    data_.Push(node);
  }

  std::optional<T> TryPop() {
    Node* node = data_.TryPop();
    if (node == nullptr) {
      return std::nullopt;
    } else {
      T popped_value = std::move(node->value);
      allocator_.Free(node);
      return popped_value;
    }
  }

 private:
  NodeStack data_;
  NodeAllocator allocator_;
};

}  // namespace lockfree

// A-B-A problem
// https://en.wikipedia.org/wiki/ABA_problem

// data: ->A->B->C
// pool: ->

// T1: curr_top = A
// T1: curr_top->next = B

// T2: TryPop(): A
// T2: TryPop(): B

// data: ->C
// pool: ->B->A

// T3: Push
//    pool.TryPop(): B

// T2: Push()
//    pool.TryPop(): A
//    data.Push(A)

// data: ->A->C
// pool: ->

// T1: top_.compare_exchange_weak(A, B)
