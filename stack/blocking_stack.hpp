#pragma once

#include <twist/stdlike/mutex.hpp>

#include <stack>

namespace lockfree {

// Not lock-free
template <typename T>
class BlockingStack {
 public:
  void Push(T value) {
    std::lock_guard guard(mutex_);
    items_.push(std::move(value));
  }

  std::optional<T> TryPop() {
    std::lock_guard guard(mutex_);
    if (!items_.empty()) {
      T top(std::move(items_.top()));
      items_.pop();
      return top;
    } else {
      return std::nullopt;
    }
  }

 private:
  twist::stdlike::mutex mutex_;
  std::stack<T> items_;
};

}  // namespace lockfree
