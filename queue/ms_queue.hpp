#pragma once

#include <twist/stdlike/atomic.hpp>

#include <optional>

namespace lockfree {

// Michael-Scott unbounded lock-free queue
// https://www.cs.rochester.edu/~scott/papers/1996_PODC_queues.pdf

// Queue: dummy -> node1 -> node2 -> node3
// head_ -> dummy
// tail_ -> node3

template <typename T>
class MSQueue {
  struct Node {
    std::optional<T> value;
    twist::stdlike::atomic<Node*> next{nullptr};
  };

 public:
  MSQueue() {
    Node* dummy = new Node{};
    head_.store(dummy);
    tail_.store(dummy);
  }

  ~MSQueue() {
    DeleteList(head_.load());
  }

  void Push(T value) {
    auto new_node = new Node{std::move(value)};

    // Step 1: add new node to the tail

    Node* curr_tail;
    while (true) {
      curr_tail = tail_.load();

      if (curr_tail->next != nullptr) {  // Concurrent Push
        // Helping!
        tail_.compare_exchange_weak(curr_tail, curr_tail->next);
        continue;
      }

      Node* null = nullptr;
      if (curr_tail->next.compare_exchange_weak(null, new_node)) {
        break;
      }
    }

    // Step 2: move tail

    tail_.compare_exchange_strong(curr_tail, new_node);
    //tail_.store(new_node);
  }

  std::optional<T> TryPop() {
    while (true) {
      Node* curr_head = head_.load();

      if (curr_head->next == nullptr) {
        // Just dummy node => empty queue
        return std::nullopt;
      }

      // Similar to TryPop in stack
      if (head_.compare_exchange_weak(curr_head, curr_head->next)) {
        // NB: skip dummy head!
        Node* popped_head = curr_head->next.load();

        // Innocent prank?
        curr_head->next.store((Node*)128);

        T popped_value = std::move(*(popped_head->value));
        // delete curr_head?
        return popped_value;
      }
    }
  }

 private:
  static void DeleteList(Node* head) {
    while (head) {
      Node* to_delete = head;
      head = head->next;
      delete to_delete;
    }
  }

 private:
  twist::stdlike::atomic<Node*> head_{nullptr};
  twist::stdlike::atomic<Node*> tail_{nullptr};
};

}  // namespace lockfree
